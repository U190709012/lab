public class FindGrade{
	public static void main(String[] args){
		if(args.length == 1){
		int not = Integer.parseInt(args[0]);
		if (not<0 || not>100) System.out.println("It's not a valid score!");
        else if (not < 60){
            System.out.println("Your grade is F");
        }
        else if (not < 70) {
            System.out.println("Your grade is D");                    
        }
        else if (not < 80) {
            System.out.println("Your grade is C");                    
        }
        else if (not < 90) {
            System.out.println("Your grade is B");
        }
        else if (not < 100) {
            System.out.println("Your grade is A");                    
        }
		}
    }
}