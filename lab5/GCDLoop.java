public class GCDLoop{
	public static void main(String[] args){
		int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);
		
		int result = gcd(num1, num2);
		System.out.println("GCD of "+ num1 +" and " + num2 + " = " + result);
		
	}
	private static int gcd(int int1, int int2){
		int remainder;
		do{
		remainder = int1 % int2;
		int1 = int2;
		int2=remainder;}while(remainder != 0);
		return int1;
	}
}