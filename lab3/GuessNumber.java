import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		int ct = 0;
		int guess;
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.println("Can you guess it: ");
		do {
			if(ct!=0){System.out.println("Sorry!");
			System.out.println("Type -1 to quit or guess another:");}
			
		guess = reader.nextInt(); //Read the user input
		if(guess == -1)break;
		else if(guess < number) System.out.println("Mine ise greater than your guess.");
		else if(guess > number) System.out.println("Mine ise less than your guess.");
		++ct;} while(guess != number);
		if(guess==number)System.out.println("Congratulations! You won after "+String.valueOf(ct)+" attempts!");
		
		reader.close(); //Close the resource before exiting
	}

	
}